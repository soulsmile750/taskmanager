<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProjectController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('dashboard', PostController::class)->middleware(['auth']);

Route::get('/', function () {
    return view('welcome');
});

Route::resource('project', ProjectController::class)->middleware(['auth']);

Route::get('/tasks', [PostController::class, 'tasks'])->middleware(['auth']);
Route::get('/show', [PostController::class, 'show'])->middleware(['auth']);
Route::get('/projects', [ProjectController::class, 'index'])->middleware(['auth']);
Route::get('/create', [ProjectController::class, 'create'])->middleware(['auth']);
//Route::get('project/edit', [ProjectController::class, 'edit'])->middleware(['auth']);
//Route::get('project/update', [ProjectController::class, 'update'])->middleware(['auth']);
Route::post('/store', [ProjectController::class, 'store'])->middleware(['auth']);



//Route::get('/dashboard', [PostController::class, 'index' ])->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
