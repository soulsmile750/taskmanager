<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
       // var_dump($posts);
        return view('dashboard', compact('posts'));

    }

    public function tasks()
    {
        $posts = Post::all();
        return view('posts.tasks', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->all();
        $id = $id['id'];
        $user = auth()->user();
        $user = $user['email'];
        return view('posts.create', compact('user', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'email' => 'required',
            'status' => 'required',
            'message' => 'required',
            'idproj' => 'required',
        ]);

        Post::create($request->all());

        return redirect()->route('dashboard.index')->with('success', 'Post created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $dashboard)
    {
        return view('posts.show', compact('dashboard'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $dashboard)
    {   
        return view('posts.edit', compact('dashboard'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $dashboard)
    {
        $request->validate([
            'email' => 'required',
            'status' => 'required',
            'message' => 'required',
        ]);

        $dashboard->update($request->all());

        return redirect()->route('dashboard.index')->with('success', 'Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $dashboard)
    {
        $dashboard ->delete();

        return redirect()->route('dashboard.index')
                        ->with('success', 'post deleted successfully');
    }
}
