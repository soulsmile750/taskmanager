@extends('layout')
@section('main_content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Post</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="/tasks"> Back</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('dashboard.store') }}" method="POST">
    @csrf

     <div class="row">


        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <strong>Message:</strong>
                <textarea class="form-control" style="height:150px" name="message" placeholder="Message"></textarea>
            </div>
        </div>
        <input type="hidden" name="email" value="{{ $user }}">
        <input type="hidden" name="status" value="new">

        <input type="hidden" name="idproj" value="{{ $id }}">
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection