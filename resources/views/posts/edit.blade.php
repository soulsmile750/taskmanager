@extends('layout')
@section('main_content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Post</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="/tasks"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('dashboard.update',$dashboard->id) }}" method="POST">
        @csrf

        @method('PUT')
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Email:</strong>
                    <input type="text" name="email" value="{{ $dashboard->email }}" class="form-control" placeholder="Email">
                </div>
            </div>

           

            <div class="col-xs-12 col-sm-12 col-md-12">
            
                <div class="form-check">
                    @if ($dashboard->status == "new")
                    <input class="form-check-input" type="radio" name="status" id="status1" value="new" checked>
                    @else
                    <input class="form-check-input" type="radio" name="status" id="status1" value="new">
                    @endif
                    <label class="form-check-label" for="status1">
                        New
                    </label>
                </div>
                <div class="form-check">
                    @if ($dashboard->status == "in progress")
                    <input class="form-check-input" type="radio" name="status" id="status2" value="in progress" checked>
                    @else
                    <input class="form-check-input" type="radio" name="status" id="status2" value="in progress">
                    @endif
                    <label class="form-check-label" for="status2">
                        in progress
                    </label>
                </div>
                <div class="form-check">
                    @if ($dashboard->status == "done")
                    <input class="form-check-input" type="radio" name="status" id="status3" value="done" checked>
                    @else
                    <input class="form-check-input" type="radio" name="status" id="status3" value="done">
                    @endif
                    <label class="form-check-label" for="status3">
                        done
                    </label>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Message:</strong>
                    <textarea class="form-control" style="height:150px" name="message" placeholder="Message">{{ $dashboard->message }}</textarea>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection