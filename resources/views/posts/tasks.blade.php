@extends('layout')
@section('main_content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Список задач </h2>
            </div>
            
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>Email</th>
            <th>Status</th>
            <th>Message</th>
        </tr>
        @foreach ($posts as $post)
        <tr>
            <td>{{ $post->email }}</td>
            <td>{{ $post->status }}</td>
            <td>{{ $post->message }}</td>
            <td><form action="{{ route('dashboard.destroy',$post->id) }}" method="POST">
                 <a class="btn btn-info" href="{{ route('dashboard.show',$post->id) }}">Show</a>
                    <a class="btn btn-primary" href="{{ route('dashboard.edit',$post->id) }}">Edit</a>
                
   
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    
@endsection