<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css">
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Список проектов </h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="/create"> Создать новый</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>Email</th>
            <th>Project name</th>
        </tr>
        @foreach ($projects as $project)
        <tr>
            <td>{{ $project->email }}</td>
            <td>{{ $project->name }}</td>
            <td><form action="{{ route('project.destroy',$project->id) }}" method="POST">
                 <a class="btn btn-info" href="{{ route('project.show',$project->id) }}">Show</a>
                    <a class="btn btn-primary" href="{{ route('project.edit',$project->id) }}">Edit</a>
                
   
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>


                </div>
            </div>
        </div>
    </div>
</x-app-layout>
