@extends('layout')
@section('main_content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Project</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="/projects"> Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                {{ $project->email }}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $project->name }}
            </div>
        </div>

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Список задач </h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('dashboard.create', ['id'=>$project->id]) }}"> Создать новую</a>
            </div>
        </div>
    </div>

    <table class="table table-bordered">
        <tr>
            <th>Email</th>
            <th>Status</th>
            <th>Message</th>
        </tr>
        @foreach ($posts as $post)
        @if ($post->idproj == $project->id)

        <tr>
            <td>{{ $post->email }}</td>
            <td>{{ $post->status }}</td>
            <td>{{ $post->message }}</td>
            <td><form action="{{ route('dashboard.destroy',$post->id) }}" method="POST">
                 <a class="btn btn-info" href="{{ route('dashboard.show',$post->id) }}">Show</a>
                    <a class="btn btn-primary" href="{{ route('dashboard.edit',$post->id) }}">Edit</a>
                
   
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endif
        @endforeach
    </table>


                </div>
            </div>
        </div>
    </div>











    </div>
@endsection