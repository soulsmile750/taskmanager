<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-bottom">
      <a href="/" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none">
        <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"></use></svg>
      </a>

      <div class="col-md-3 text-end">
        @if (Route::has('login'))
        @auth
          <a href="/dashboard" class="btn btn-outline-primary me-2">Dashboard</a>
          <a href="/projects" class="btn btn-outline-primary me-2">Projects</a>
        @else
          <a href="login" class="btn btn-outline-primary me-2">Login</a>
        @if (Route::has('register'))
          <a href="register" class="btn btn-primary">Register</a>
        @endif
        @endauth
        @endif
      </div>
    </header>
    @yield('main_content')
	</div>



</body>
</html>